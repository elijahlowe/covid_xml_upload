aws s3 ls s3://ncezid-non-pii/ --recursive | grep bam$ | cut -c 32- > bam_files.txt
aws s3 ls s3://ncezid-non-pii/ --recursive | grep fastq.gz$ | cut -c 32- > fastq_files.txt
