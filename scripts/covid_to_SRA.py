import pandas as pd
from lxml import etree as ET
from datetime import datetime, timedelta, date
#from dateutil.relativedelta import relativedelta


class ncbi_quest_metadata:
    """
    Function to extract and transform biosample and metadata to xml format using hadoop, metadata and s3 bucket location files. 
    
    args:
        hadoop_file: The hadoop csv containing data from the runs
        metadata: The metadata file generated from the runs
        loc_file: The bam_file.txt or fastq_files.txt location file generated from running generate_files.sh
        ftype: if the sequencing files are `bam` or `fastq`
        mtype: if the metadata file is `csv` or `tsv`
    """
    def __init__(self, hadoop_file, metadata, loc_file='./../sars-cov-2-upload/fastq_files.txt',ftype='fastq',mtype='csv'):
        self.hadoop = hadoop_file
        self.metadata = metadata
        self.files = loc_file
        self.ftype = ftype
        self.mtype = mtype

    def read(self):
        """
        Combineds hadoop and metadata files into a pandas dataframe and adds a column with the s3 bucket file locations.
        """
        hadoop =  pd.read_csv(self.hadoop, dtype='string')
        if self.mtype == 'csv':
            metadata = pd.read_csv(self.metadata, dtype='string')
        elif self.mtype == 'tsv':
            metadata = pd.read_csv(self.metadata, dtype='string', sep='\t')
    #hadoop['contractor_sample_name_old'] = hadoop['contractor_sample_name_old'].astype('string')
    #quest_m['ACCESSION_NUMBER'] = quest_m['ACCESSION_NUMBER'].astype('string')
        quest = hadoop.join(metadata.set_index('ACCESSION_NUMBER'), on='contractor_sample_name_old')
        if self.ftype == 'fastq':
            file_loc = pd.read_csv(self.files, header=None)
            file_loc.rename({0:"loc"}, axis=1, inplace=True)
            file_loc = file_loc[file_loc['loc'].str.contains('quest')]
            file_loc["pre"] = "s3://ncezid-non-pii"
            file_loc["seq_barcode"] = file_loc["loc"].str.split("/", expand=True)[7].str.split("_",expand=True)[0]
            file_loc["seq_barcode"] = file_loc["seq_barcode"].str.split("_",expand=True)[0]
            file_loc["seq_barcode"] = file_loc["seq_barcode"].str.split("-",expand=True)[2]
            file_loc['loc'] = file_loc['pre'].str.cat(file_loc['loc'],sep="/")
            file_loc.drop(['pre'], axis=1, inplace=True)
            locs = file_loc.groupby('seq_barcode').agg(lambda x: x.tolist())
            locs.reset_index(inplace=True)
            locs[['filename','filename2']] = pd.DataFrame(locs['loc'].tolist(), index= locs.index)
        elif self.ftype == 'bam':
            file_loc = pd.read_csv(self.files, header=None)
            file_loc.rename({0:"loc"}, axis=1, inplace=True)
            file_loc = file_loc[file_loc['loc'].str.contains('quest')]
            file_loc["pre"] = "s3://ncezid-non-pii"
            file_loc["seq_barcode"] = file_loc["loc"].str.split("/", expand=True)[6]
            file_loc['loc'] = file_loc['pre'].str.cat(file_loc['loc'],sep="/")
            file_loc.drop(['pre'], axis=1, inplace=True)
        #quest['contractor_sample_name_old'] = quest['contractor_sample_name_old'].astype('string')
        #locs['seq_barcode'] = locs['seq_barcode'].astype('string')
    #quest['ACCESSION_NUMBER'] = quest['ACCESSION_NUMBER'].astype('string')
        quest.fillna('missing', inplace=True)
        quest = quest.join(locs.set_index('seq_barcode'), on='contractor_sample_name_old',how='left')
        quest['contractor_country_only'] = quest['contractor_country_only'].str.replace("united States", "USA", case=False)
        return quest
    
#    today = date.today()
#    today = today.strftime("%d-%m-%Y")

# create the file structure
    def cov_2_xml(self):
        """
        Writes xml file containing biosample data and metadata to standard out. BioProject is hard coded.
        """
        day = str(date.today()) #'12/Oct/2013'
        dt = datetime.strptime(day, '%Y-%m-%d')
        today = dt + timedelta(days=14)
        release = today.strftime("%Y-%m-%d")
        xml_test = ncbi_quest_metadata.read(self)
        submission = ET.Element('Submission')
        description = ET.SubElement(submission, 'Description')
        title = ET.SubElement(description, 'Title')
        title.text = 'SARS-CoV-2 Genome sequencing and assembly - QUEST'+"_".join(self.hadoop.split('/')[11].split('_')[0:2])
        comment = ET.SubElement(description, 'Comment')
        comment.text = 'BP(1.0)+BS(1.0)+SRA'
        org = ET.SubElement(description, 'Organization')
        org.set('role', 'owner')
        org.set('type','institute')
        org.set('org_id', "20768")
        name = ET.SubElement(org, 'Name')
        email = ET.SubElement(org, 'Contact')
        email.set('email', 'qdy1@cdc.gov')
        name.text = 'Centers for Disease Control and Prevention'
        name = ET.SubElement(email, 'Name')
        first = ET.SubElement(name, 'First')
        last = ET.SubElement(name, 'Last')
        first.text = "Elijah"
        last.text = "Lowe"
        hold = ET.SubElement(description, 'Hold')
        hold.set('release_date', str(release))
        #action = ET.SubElement(submission, 'Action')
        #addData = ET.SubElement(action, 'AddData')
        #addData.set('target_db',"BioProject")
        #data_tag = ET.SubElement(addData, 'Data')
        #data_tag.set('content_type',"xml")
        #xmlC = ET.SubElement(data_tag, 'XmlContent')
        #pro_s = ET.SubElement(xmlC, 'Project')
        #pro_s.set('schema_version',"2.0")
        #proID = ET.SubElement(pro_s, 'ProjectID')
        #spuid = ET.SubElement(proID, 'SPUID')
        #spuid.set('spuid_namespace',"CDC-OAMD")
        #spuid.text = 'PRJNA716985'
        #des = ET.SubElement(pro_s, 'Descriptor')
        #title = ET.SubElement(des, 'Title')
        #title.text = 'Sars-coV-2: CDC samples from Quest'
        #desion = ET.SubElement(des, 'Description')
        #p = ET.SubElement(desion, 'p')
        #p.text = 'Sars-cov-2 samples from human host Genome sequencing and assembly'
        ##link = ET.SubElement(des, 'ExternalLink')
        ##link.set("label","Website: Epidemiological Research Center at Sinaloa, Mexico")
        ##url = ET.SubElement(link, 'URL')
        ##url.text = 'http://www.hgculiacan.com/cies/cies%20descripcion.htm'
        #rel = ET.SubElement(des, 'Relevance')
        #med = ET.SubElement(rel, 'Medical')
        #med.text = 'Yes'
        #proT = ET.SubElement(pro_s, 'ProjectType')
        #proTS = ET.SubElement(proT, 'ProjectTypeSubmission')
        #proTS.set('sample_scope',"eMonoisolate")
        #intend = ET.SubElement(proTS, 'IntendedDataTypeSet')
        #DT = ET.SubElement(intend, 'DataType')
        #DT.text = 'genome sequencing'
        #id = ET.SubElement(addData, 'Identifier')
        #spuid = ET.SubElement(id, 'SPUID')
        #spuid.set('spuid_namespace',"CDC-OAMD")
        #spuid.text = 'Sars-CoV-2_Quest'
		
        #Action block 2 = biosample data
        for index, xml in xml_test.iterrows():
            action2 = ET.SubElement(submission, 'Action')
            addData = ET.SubElement(action2, 'AddData')
            addData.set('target_db',"BioSample")
            data_tag = ET.SubElement(addData, 'Data')
            data_tag.set('content_type',"xml")
            xmlC = ET.SubElement(data_tag, 'XmlContent')
            bioS = ET.SubElement(xmlC, 'BioSample')
            bioS.set('schema_version',"2.0")
            samID = ET.SubElement(bioS, 'SampleId')
            spuid = ET.SubElement(samID, 'SPUID')
            spuid.set('spuid_namespace',"CDC-OAMD")
            spuid.text = xml['contractor_virus_name_ncbi'].str.replace('CDC-', 'CDC-QDX')
            des = ET.SubElement(bioS, 'Descriptor')
            title = ET.SubElement(des, 'Title')
            title.text = 'SARS-CoV-2 Genome sequencing and assembly - QUEST'
            org = ET.SubElement(bioS, 'Organism')
            orgName = ET.SubElement(org, 'OrganismName')
            orgName.text = "Severe acute respiratory syndrome coronavirus 2"
            biop = ET.SubElement(bioS, 'BioProject')
            primeID = ET.SubElement(biop, 'PrimaryId')
            primeID.set('db',"BioProject")
            primeID.text = 'PRJNA716985'
            pack = ET.SubElement(bioS, 'Package')
            pack.text = 'SARS-CoV-2.cl.1.0'
            attr = ET.SubElement(bioS,'Attributes')
            attr_str = ET.SubElement(attr, 'Attribute')
            attr_str.set('attribute_name', "Isolate")
            attr_str.text = xml['contractor_virus_name_ncbi']
            attr_by = ET.SubElement(attr, 'Attribute')
            attr_by.set('attribute_name', "collected_by")
            attr_by.text = xml["SEQ_SUBMITTED_BY"]
            attr_date = ET.SubElement(attr, 'Attribute')
            attr_date.set('attribute_name', "collection_date")
            attr_date.text = xml["contractor_collection_date"]
            attr_iosSource = ET.SubElement(attr, 'Attribute')
            attr_iosSource.set('attribute_name', "isolation_source")
            attr_iosSource.text = xml["contractor_specimen"].split(" ")[0]
            attr_geo = ET.SubElement(attr, 'Attribute')
            attr_geo.set('attribute_name', "geo_loc_name")
            attr_geo.text = xml['contractor_country_only']+':'+xml['contractor_stateFull']
            attr_lon_lat = ET.SubElement(attr, 'Attribute')
            attr_lon_lat.set('attribute_name', "lat_lon")
            attr_lon_lat.text = 'missing'
            attr_host = ET.SubElement(attr, 'Attribute')
            attr_host.set('attribute_name', "host")
            attr_host.text = 'Homo-Sapiens'#xml["contractor_host"]
            attr_hostD = ET.SubElement(attr, 'Attribute')
            attr_hostD.set('attribute_name', "host_disease")
            attr_hostD.text = "COVID-19"
            attr_hostS = ET.SubElement(attr, 'Attribute')
            attr_hostS.set('attribute_name', "host_sex")
            attr_hostS.text = xml["contractor_sex"]
            attr_hostA = ET.SubElement(attr, 'Attribute')
            attr_hostA.set('attribute_name', "host_age")
            attr_hostA.text = xml["contractor_age"]
            attr_hostR = ET.SubElement(attr, 'Attribute')
            attr_hostR.set('attribute_name', ",host_race")
            attr_hostR.text = xml["contractor_race"]
            attr_hostAP = ET.SubElement(attr, 'Attribute')
            attr_hostAP.set('attribute_name', "host_anatomical_part")
            attr_hostAP.text = xml["contractor_specimen"].split(" ")[0]
            attr_colD = ET.SubElement(attr, 'Attribute')
            attr_colD.set('attribute_name', "collection_device")
            attr_colD.text = xml["contractor_specimen"].split(" ")[1]
            attr_colM = ET.SubElement(attr, 'Attribute')
            attr_colM.set('attribute_name', "collection_method")
            attr_colM.text = xml["contractor_specimen"].split(" ")[1]
            attr_diaG = ET.SubElement(attr, 'Attribute')
            attr_diaG.set('attribute_name', "diagnostic_gene_name_1")
            attr_diaG.text = xml["PURPOSE_OF_SAMPLING"]
            attr_diaP = ET.SubElement(attr, 'Attribute')
            attr_diaP.set('attribute_name', "diagnostic_pcr_protocol_1")
            attr_diaP.text = xml["PLATFORM"]
            attr_diaCt = ET.SubElement(attr, 'Attribute')
            attr_diaCt.set('attribute_name', "diagnostic_pcr_Ct_value_1")
            attr_diaCt.text = xml["CT_VALUE"]
            attr_clade = ET.SubElement(attr, 'Attribute')
            attr_clade.set('attribute_name', "lineage/clade name")
            attr_clade.text = xml["contractor_lineageCDCgen"]
            
            id = ET.SubElement(addData, 'Identifier')
            spuid = ET.SubElement(id, 'SPUID')
            spuid.set('spuid_namespace',"CDC-OAMD")
            spuid.text = xml['contractor_virus_name_ncbi'].str.replace('CDC-', 'CDC-QDX')


        for index, xml in xml_test.iterrows():
            action3 = ET.SubElement(submission, 'Action')
            addfile = ET.SubElement(action3, 'AddFiles')
            addfile.set('target_db',"SRA")
            file = ET.SubElement(addfile, 'File')
            file.set('cloud_url', xml['filename'])
            datatype=ET.SubElement(file, "DataType")
            datatype.text = 'generic-data'
            file = ET.SubElement(addfile, 'File')
            file.set('cloud_url', xml['filename2'])
            datatype=ET.SubElement(file, "DataType")
            datatype.text = 'generic-data'
            Attr_model = ET.SubElement(addfile, 'Attribute')
            Attr_model.set('name',"instrument_model")
            Attr_model.text = xml['SEQ_INSTRUMENT']
            Attr_name = ET.SubElement(addfile, 'Attribute')
            Attr_name.set('name',"library_name")
            Attr_name.text = xml['contractor_virus_name_ncbi']
            Attr_strat = ET.SubElement(addfile, 'Attribute')
            Attr_strat.set('name',"library_strategy")
            Attr_strat.text = 'AMPLICON'
            Attr_source = ET.SubElement(addfile, 'Attribute')
            Attr_source.set('name',"library_source")
            Attr_source.text = 'VIRAL RNA'
            Attr_select = ET.SubElement(addfile, 'Attribute')
            Attr_select.set('name',"library_selection")
            Attr_select.text = 'RT-PCR'
            Attr_lay = ET.SubElement(addfile, 'Attribute')
            Attr_lay.set('name',"library_layout")
            Attr_lay.text = 'PAIRED'
            Attr_prot = ET.SubElement(addfile, 'Attribute')
            Attr_prot.set('name',"library_construction_protocol")
            Attr_prot.text = 'idk' #xml_test['design_description'][0]
            
            Attr_ref = ET.SubElement(addfile, 'AttributeRefId')
            Attr_ref.set("name","BioProject")
            refid = ET.SubElement(Attr_ref, "RefId")
            primid = ET.SubElement(refid, 'PrimaryId')
            primid.text = "PRJNA716985"
            Attr_ref = ET.SubElement(addfile, 'AttributeRefId')
            Attr_ref.set("name","BioSample")
            refid = ET.SubElement(Attr_ref, "RefId")
            spuid = ET.SubElement(refid, 'SPUID')
            spuid.set('spuid_namespace',"CDC-OAMD")
            spuid.text = xml['contractor_virus_name_ncbi'].str.replace('CDC-', 'CDC-QDX')
            id = ET.SubElement(addfile, 'Identifier')
            spuid = ET.SubElement(id, 'SPUID')
            spuid.set('spuid_namespace',"CDC-OAMD")
            spuid.text = xml['contractor_vendor_id']

        return(ET.tostring(submission, pretty_print=True).decode())


###################################################################
                 ##  ##  #####  ##        ####  ##   ##
                 ##  ##  ##     ##         ##    ## ##
                 ######  ####   ##         ##     ###
                 ##  ##  ##     ##         ##    ## ##
                 ##  ##  #####  #######   ####  ##   ##
####################################################################

class ncbi_helix_metadata:
    """
    Function to extract and transform biosample and metadata to xml format using hadoop, metadata and s3 bucket location files. 
    
    args:
        hadoop_file: The hadoop csv containing data from the runs
        metadata: The metadata file generated from the runs
        loc_file: The bam_file.txt or fastq_files.txt location file generated from running generate_files.sh
        ftype: if the sequencing files are `bam` or `fastq`
        mtype: if the metadata file is `csv` or `tsv`
    """
    def __init__(self, hadoop_file, metadata, loc_file='./../sars-cov-2-upload/bam_files.txt',ftype='fastq',mtype='csv'):
        self.hadoop = hadoop_file
        self.metadata = metadata
        self.files = loc_file
        self.ftype = ftype
        self.mtype = mtype

    def read(self):
        """
        Combineds hadoop and metadata files into a pandas dataframe and adds a column with the s3 bucket file locations.
        """
        helix = pd.read_csv(self.hadoop, dtype='string')
        helix_m = pd.read_csv(self.metadata, sep='\t', dtype='string')
        helix = helix.join(helix_m.set_index('seq_barcode'), on='contractor_sample_name_old', how='left')
        if self.ftype == 'fastq':
            file_loc = pd.read_csv(self.files, header=None)
            file_loc.rename({0:"loc"}, axis=1, inplace=True)
            file_loc = file_loc[file_loc['loc'].str.contains('illumina')]
            file_loc["pre"] = "s3://ncezid-non-pii"
            file_loc["seq_barcode"] = file_loc["loc"].str.split("/", expand=True)[7].str.split("_",expand=True)[0]
            #file_loc["seq_barcode"] = file_loc["seq_barcode"].str.split("_",expand=True)[0]
            #file_loc["seq_barcode"] = file_loc["seq_barcode"].str.split("-",expand=True)[2]
            file_loc['loc'] = file_loc['pre'].str.cat(file_loc['loc'],sep="/")
            file_loc.drop(['pre'], axis=1, inplace=True)
            locs = file_loc.groupby('seq_barcode').agg(lambda x: x.tolist())
            locs.reset_index(inplace=True)
            locs[['filename','filename2']] = pd.DataFrame(locs['loc'].tolist(), index= locs.index)
            file_loc = locs
        elif self.ftype == 'bam':
            file_loc = pd.read_csv(self.files, header=None)
            file_loc.rename({0:"loc"}, axis=1, inplace=True)
            file_loc = file_loc[file_loc['loc'].str.contains('illumina')]
            file_loc["pre"] = "s3://ncezid-non-pii"
            file_loc["seq_barcode"] = file_loc["loc"].str.split("/", expand=True)[6]
            file_loc['loc'] = file_loc['pre'].str.cat(file_loc['loc'],sep="/")
            file_loc.drop(['pre'], axis=1, inplace=True)
            file_loc.rename({"loc":"filename"}, axis=1, inplace=True)
            file_loc = file_loc.groupby(['seq_barcode'])['filename'].first().reset_index()
        helix.fillna('', inplace=True)
        helix = helix.join(file_loc.set_index('seq_barcode'), on='contractor_sample_name_old', how='left')
        helix['contractor_country_only']=helix['contractor_country_only'].str.replace("united States", "USA", case=False)
        return(helix)

    def cov_2_xml(self):
        """
        Writes xml file containing biosample data and metadata to standard out. BioProject is hard coded.
        """
        day = str(date.today()) #'12/Oct/2013'
        dt = datetime.strptime(day, '%Y-%m-%d')
        today = dt + timedelta(days=14)
        release = today.strftime("%Y-%m-%d")
        xml_test = ncbi_helix_metadata.read(self)
        submission = ET.Element('Submission')
        description = ET.SubElement(submission, 'Description')
        title = ET.SubElement(description, 'Title')
        title.text = 'SARS-CoV-2 Genome sequencing and assembly - HELIX'+"_".join(self.hadoop.split('/')[11].split('_')[0:2])
        comment = ET.SubElement(description, 'Comment')
        comment.text = 'BP(1.0)+BS(1.0)+SRA'
        org = ET.SubElement(description, 'Organization')
        org.set('role', 'owner')
        org.set('type','institute')
        name = ET.SubElement(org, 'Name')
        email = ET.SubElement(org, 'Contact')
        email.set('email', 'qdy1@cdc.gov')
        name.text = 'Centers for Disease Control and Prevention'
        name = ET.SubElement(email, 'Name')
        first = ET.SubElement(name, 'First')
        last = ET.SubElement(name, 'Last')
        first.text = "Elijah"
        last.text = "Lowe"
        hold = ET.SubElement(description, 'Hold')
        hold.set('release_date', release)
        #action = ET.SubElement(submission, 'Action')
        #addData = ET.SubElement(action, 'AddData')
        #addData.set('target_db',"BioProject")
        #data_tag = ET.SubElement(addData, 'Data')
        #data_tag.set('content_type',"xml")
        #xmlC = ET.SubElement(data_tag, 'XmlContent')
        #pro_s = ET.SubElement(xmlC, 'Project')
        #pro_s.set('schema_version',"2.0")
        #proID = ET.SubElement(pro_s, 'ProjectID')
        #spuid = ET.SubElement(proID, 'SPUID')
        #spuid.set('spuid_namespace',"CDC-OAMD")
        #spuid.text = 'PRJNA720050'
        #des = ET.SubElement(pro_s, 'Descriptor')
        #title = ET.SubElement(des, 'Title')
        #title.text = 'Sars-coV-2: CDC samples from Helix'
        #desion = ET.SubElement(des, 'Description')
        #p = ET.SubElement(desion, 'p')
        #p.text = 'Sars-cov-2 samples from human host Genome sequencing and assembly'
        ##link = ET.SubElement(des, 'ExternalLink')
        ##link.set("label","Website: Epidemiological Research Center at Sinaloa, Mexico")
        ##url = ET.SubElement(link, 'URL')
        ##url.text = 'http://www.hgculiacan.com/cies/cies%20descripcion.htm'
        #rel = ET.SubElement(des, 'Relevance')
        #med = ET.SubElement(rel, 'Medical')
        #med.text = 'Yes'
        #proT = ET.SubElement(pro_s, 'ProjectType')
        #proTS = ET.SubElement(proT, 'ProjectTypeSubmission')
        #proTS.set('sample_scope',"eMonoisolate")
        #intend = ET.SubElement(proTS, 'IntendedDataTypeSet')
        #DT = ET.SubElement(intend, 'DataType')
        #DT.text = 'genome sequencing'
        #id = ET.SubElement(addData, 'Identifier')
        #spuid = ET.SubElement(id, 'SPUID')
        #spuid.set('spuid_namespace',"CDC-OAMD")
        #spuid.text = 'Sars-CoV-2_Helix'
		
        #helix Action block 2 = biosample data
        for index, xml in xml_test.iterrows():
            action2 = ET.SubElement(submission, 'Action')
            addData = ET.SubElement(action2, 'AddData')
            addData.set('target_db',"BioSample")
            data_tag = ET.SubElement(addData, 'Data')
            data_tag.set('content_type',"xml")
            xmlC = ET.SubElement(data_tag, 'XmlContent')
            bioS = ET.SubElement(xmlC, 'BioSample')
            bioS.set('schema_version',"2.0")
            samID = ET.SubElement(bioS, 'SampleId')
            spuid = ET.SubElement(samID, 'SPUID')
            spuid.set('spuid_namespace',"CDC-OAMD")
            spuid.text = xml['contractor_virus_name_ncbi']
            des = ET.SubElement(bioS, 'Descriptor')
            title = ET.SubElement(des, 'Title')
            title.text = 'SARS-CoV-2 Genome sequencing and assembly - HELIX'
            org = ET.SubElement(bioS, 'Organism')
            orgName = ET.SubElement(org, 'OrganismName')
            orgName.text = "Severe acute respiratory syndrome coronavirus 2"
            biop = ET.SubElement(bioS, 'BioProject')
            primeID = ET.SubElement(biop, 'PrimaryId')
            primeID.set('db',"BioProject")
            primeID.text = 'PRJNA720050'
            pack = ET.SubElement(bioS, 'Package')
            pack.text = 'SARS-CoV-2.cl.1.0'
            attr = ET.SubElement(bioS,'Attributes')
            attr_str = ET.SubElement(attr, 'Attribute')
            attr_str.set('attribute_name', "Isolate")
            attr_str.text = xml['contractor_virus_name_ncbi']
            attr_by = ET.SubElement(attr, 'Attribute')
            attr_by.set('attribute_name', "collected_by")
            attr_by.text = "Helix"
            attr_date = ET.SubElement(attr, 'Attribute')
            attr_date.set('attribute_name', "collection_date")
            attr_date.text = xml["contractor_collection_date"]
            attr_iosSource = ET.SubElement(attr, 'Attribute')
            attr_iosSource.set('attribute_name', "isolation_source")
            attr_iosSource.text = xml["contractor_specimen"].split(" ")[0]
            attr_geo = ET.SubElement(attr, 'Attribute')
            attr_geo.set('attribute_name', "geo_loc_name")
            attr_geo.text = xml['contractor_country_only']+':'+xml['contractor_stateFull']
            attr_lon_lat = ET.SubElement(attr, 'Attribute')
            attr_lon_lat.set('attribute_name', "lat_lon")
            attr_lon_lat.text = 'missing'
            attr_host = ET.SubElement(attr, 'Attribute')
            attr_host.set('attribute_name', "host")
            attr_host.text = 'Homo-Sapiens'#xml["contractor_host"]
            attr_hostD = ET.SubElement(attr, 'Attribute')
            attr_hostD.set('attribute_name', "host_disease")
            attr_hostD.text = "COVID-19"
            attr_hostS = ET.SubElement(attr, 'Attribute')
            attr_hostS.set('attribute_name', "host_sex")
            attr_hostS.text = xml["contractor_sex"]
            attr_hostA = ET.SubElement(attr, 'Attribute')
            attr_hostA.set('attribute_name', "host_age")
            attr_hostA.text = xml["contractor_age"]
            attr_hostR = ET.SubElement(attr, 'Attribute')
            attr_hostR.set('attribute_name', ",host_race")
            attr_hostR.text = xml["contractor_race"]
            attr_hostAP = ET.SubElement(attr, 'Attribute')
            attr_hostAP.set('attribute_name', "host_anatomical_part")
            attr_hostAP.text = xml["contractor_specimen"].split(" ")[0]
            attr_colD = ET.SubElement(attr, 'Attribute')
            attr_colD.set('attribute_name', "collection_device")
            attr_colD.text = xml["contractor_specimen"].split(" ")[1]
            attr_colM = ET.SubElement(attr, 'Attribute')
            attr_colM.set('attribute_name', "collection_method")
            attr_colM.text = xml["contractor_specimen"].split(" ")[1]
            if xml['cq_ms2'] != "":
                attr_diaG = ET.SubElement(attr, 'Attribute')
                attr_diaG.set('attribute_name', "diagnostic_gene_name_1")
                attr_diaG.text = "ms2"
                attr_diaP = ET.SubElement(attr, 'Attribute')
                attr_diaP.set('attribute_name', "diagnostic_pcr_protocol_1")
                attr_diaP.text = "ThermoFisher Taqpath Assay"
                attr_diaCt = ET.SubElement(attr, 'Attribute')
                attr_diaCt.set('attribute_name', "diagnostic_pcr_Ct_value_1")
                attr_diaCt.text = xml["cq_ms2"]
            else:
                pass
            if xml['cq_n_gene'] != "":
                attr_diaG2 = ET.SubElement(attr, 'Attribute')
                attr_diaG2.set('attribute_name', "diagnostic_gene_name_2")
                attr_diaG2.text = "n gene"
                attr_diaP2 = ET.SubElement(attr, 'Attribute')
                attr_diaP2.set('attribute_name', "diagnostic_pcr_protocol_2")
                attr_diaP2.text = "ThermoFisher Taqpath Assay"
                attr_diaCt2 = ET.SubElement(attr, 'Attribute')
                attr_diaCt2.set('attribute_name', "diagnostic_pcr_Ct_value_2")
                attr_diaCt2.text = xml["cq_n_gene"]
            else:
                pass
            if xml['cq_orf1ab'] != "":
                attr_diaG3 = ET.SubElement(attr, 'Attribute')
                attr_diaG3.set('attribute_name', "diagnostic_gene_name_3")
                attr_diaG3.text = 'orf1ab'
                attr_diaP3 = ET.SubElement(attr, 'Attribute')
                attr_diaP3.set('attribute_name', "diagnostic_pcr_protocol_3")
                attr_diaP3.text = "ThermoFisher Taqpath Assay"
                attr_diaCt3 = ET.SubElement(attr, 'Attribute')
                attr_diaCt3.set('attribute_name', "diagnostic_pcr_Ct_value_3")
                attr_diaCt3.text = xml["cq_orf1ab"]
            else:
                pass
            if xml['cq_s_gene'] != "":
                attr_diaG4 = ET.SubElement(attr, 'Attribute')
                attr_diaG4.set('attribute_name', "diagnostic_gene_name_4")
                attr_diaG4.text = "s gene"
                attr_diaP4 = ET.SubElement(attr, 'Attribute')
                attr_diaP4.set('attribute_name', "diagnostic_pcr_protocol_4")
                attr_diaP4.text = "ThermoFisher Taqpath Assay"
                attr_diaCt4 = ET.SubElement(attr, 'Attribute')
                attr_diaCt4.set('attribute_name', "diagnostic_pcr_Ct_value_4")
                attr_diaCt4.text = xml["cq_s_gene"]
            else:
                pass
            attr_clade = ET.SubElement(attr, 'Attribute')
            attr_clade.set('attribute_name', "lineage/clade name")
            attr_clade.text = xml["contractor_lineageCDCgen"]
            id = ET.SubElement(addData, 'Identifier')
            spuid = ET.SubElement(id, 'SPUID')
            spuid.set('spuid_namespace',"CDC-OAMD")
            spuid.text = xml['contractor_virus_name_ncbi']

        # helix action blocks 3 metadata
        for index, xml in xml_test.iterrows():
            action3 = ET.SubElement(submission, 'Action')
            addfile = ET.SubElement(action3, 'AddFiles')
            addfile.set('target_db',"SRA")
            if self.ftype == 'fastq':
                file = ET.SubElement(addfile, 'File')
                file.set('cloud_url', xml['filename'])
                datatype=ET.SubElement(file, "DataType")
                datatype.text = 'generic-data'
                file = ET.SubElement(addfile, 'File')
                file.set('cloud_url', xml['filename2'])
                datatype=ET.SubElement(file, "DataType")
                datatype.text = self.ftype
            else:
                file = ET.SubElement(addfile, 'File')
                file.set('cloud_url', xml['filename'])
                datatype=ET.SubElement(file, "DataType")
                datatype.text = 'generic-data'
                Attr_assem = ET.SubElement(addfile, 'Attribute')
                Attr_assem.set('name',"assembly")
                Attr_assem.text = "NC_045512.2"
            Attr_model = ET.SubElement(addfile, 'Attribute')
            Attr_model.set('name',"instrument_model")
            Attr_model.text = 'Illumina NovaSeq 6000'
            Attr_name = ET.SubElement(addfile, 'Attribute')
            Attr_name.set('name',"library_name")
            Attr_name.text = xml['contractor_virus_name_ncbi']
            Attr_strat = ET.SubElement(addfile, 'Attribute')
            Attr_strat.set('name',"library_strategy")
            Attr_strat.text = 'AMPLICON'
            Attr_source = ET.SubElement(addfile, 'Attribute')
            Attr_source.set('name',"library_source")
            Attr_source.text = 'VIRAL RNA'
            Attr_select = ET.SubElement(addfile, 'Attribute')
            Attr_select.set('name',"library_selection")
            Attr_select.text = 'RT-PCR'
            Attr_lay = ET.SubElement(addfile, 'Attribute')
            Attr_lay.set('name',"library_layout")
            Attr_lay.text = 'PAIRED'
            Attr_prot = ET.SubElement(addfile, 'Attribute')
            Attr_prot.set('name',"library_construction_protocol")
            Attr_prot.text = 'Amplicon Based Artic V3 COVIDSEQ' #xml_test['design_description'][0]
            Attr_ref = ET.SubElement(addfile, 'AttributeRefId')
            Attr_ref.set("name","BioProject")
            refid = ET.SubElement(Attr_ref, "RefId")
            primid = ET.SubElement(refid, 'PrimaryId')
            primid.text = "PRJNA720050"
            Attr_ref = ET.SubElement(addfile, 'AttributeRefId')
            Attr_ref.set("name","BioSample")
            refid = ET.SubElement(Attr_ref, "RefId")
            spuid = ET.SubElement(refid, 'SPUID')
            spuid.set('spuid_namespace',"CDC-OAMD")
            spuid.text = xml['contractor_virus_name_ncbi']
            id = ET.SubElement(addfile, 'Identifier')
            spuid = ET.SubElement(id, 'SPUID')
            spuid.set('spuid_namespace',"CDC-OAMD")
            spuid.text = xml['contractor_vendor_id']

        return(ET.tostring(submission, pretty_print=True).decode())

    
################################################################################


#Labcorp


################################################################################
class ncbi_labcorp_metadata:
    """
    Function to extract and transform biosample and metadata to xml format using hadoop, metadata and s3 bucket location files. 
    
    args:
        hadoop_file: The hadoop csv containing data from the runs
        metadata: The metadata file generated from the runs
        loc_file: The bam_file.txt or fastq_files.txt location file generated from running generate_files.sh
        ftype: if the sequencing files are `bam` or `fastq`
        mtype: if the metadata file is `csv` or `tsv`
    """
    def __init__(self, hadoop_file, metadata, loc_file='./../sars-cov-2-upload/bam_files.txt',ftype='bam',mtype='tsv'):
        self.hadoop = hadoop_file
        self.metadata = metadata
        self.files = loc_file
        self.ftype = ftype
        self.mtype = mtype

    def read(self):
        """
        Combineds hadoop and metadata files into a pandas dataframe and adds a column with the s3 bucket file locations.
        """
        hadoop =  pd.read_csv(self.hadoop, dtype='string')
        if self.mtype == 'csv':
            metadata = pd.read_csv(self.metadata, dtype='string')
        elif self.mtype == 'tsv':
            metadata = pd.read_csv(self.metadata, dtype='string', sep='\t')
        aegis = hadoop.join(metadata.set_index('sample_name'), on='contractor_sample_name_old')
        if self.ftype == 'fastq':
            file_loc = pd.read_csv(self.files, header=None)
            file_loc.rename({0:"loc"}, axis=1, inplace=True)
            file_loc = file_loc[file_loc['loc'].str.contains('quest')]
            file_loc["pre"] = "s3://ncezid-non-pii"
            file_loc["seq_barcode"] = file_loc["loc"].str.split("/", expand=True)[7].str.split("_",expand=True)[0]
            file_loc["seq_barcode"] = file_loc["seq_barcode"].str.split("_",expand=True)[0]
            file_loc["seq_barcode"] = file_loc["seq_barcode"].str.split("-",expand=True)[2]
            file_loc['loc'] = file_loc['pre'].str.cat(file_loc['loc'],sep="/")
            file_loc.drop(['pre'], axis=1, inplace=True)
            locs = file_loc.groupby('seq_barcode').agg(lambda x: x.tolist())
            locs.reset_index(inplace=True)
            locs[['filename','filename2']] = pd.DataFrame(locs['loc'].tolist(), index= locs.index)
        elif self.ftype == 'bam':
            file_loc = pd.read_csv(self.files, header=None)
            file_loc.rename({0:"filename"}, axis=1, inplace=True)
            file_loc = file_loc[file_loc['filename'].str.contains('labcorp')]
            file_loc["pre"] = "s3://ncezid-non-pii"
            file_loc["seq_barcode"] = file_loc["filename"].str.split("/", expand=True)[6].str.split('-',expand=True)[0]
            file_loc['filename'] = file_loc['pre'].str.cat(file_loc['filename'],sep="/")
            file_loc.drop(['pre'], axis=1, inplace=True)
            locs = file_loc
        labcorp.fillna('missing', inplace=True)
        labcorp = labcorp.join(locs.set_index('seq_barcode'), on='contractor_vendor_id',how='left')
        labcorp['contractor_country_only'] = labcorp['contractor_country_only'].str.replace("united States", "USA", case=False)
        return aegis
    
#    today = date.today()
#    today = today.strftime("%d-%m-%Y")

# create the file structure
    def cov_2_xml(self):
        """
        Writes xml file containing biosample data and metadata to standard out. BioProject is hard coded.
        """
        day = str(date.today()) #'12/Oct/2013'
        dt = datetime.strptime(day, '%Y-%m-%d')
        today = dt + timedelta(days=14)
        release = today.strftime("%Y-%m-%d")
        xml_test = ncbi_aegis_metadata.read(self)
        submission = ET.Element('Submission')
        description = ET.SubElement(submission, 'Description')
        title = ET.SubElement(description, 'Title')
        title.text = 'SARS-CoV-2 Genome sequencing and assembly - AEGIS'+"_".join(self.hadoop.split('/')[11].split('_')[0:2])
        comment = ET.SubElement(description, 'Comment')
        comment.text = 'BP(1.0)+BS(1.0)+SRA'
        org = ET.SubElement(description, 'Organization')
        org.set('role', 'owner')
        org.set('type','institute')
        org.set('org_id', "20768")
        name = ET.SubElement(org, 'Name')
        email = ET.SubElement(org, 'Contact')
        email.set('email', 'qdy1@cdc.gov')
        name.text = 'Centers for Disease Control and Prevention'
        name = ET.SubElement(email, 'Name')
        first = ET.SubElement(name, 'First')
        last = ET.SubElement(name, 'Last')
        first.text = "Elijah"
        last.text = "Lowe"
        hold = ET.SubElement(description, 'Hold')
        hold.set('release_date', str(release))
        
		
        #Action block 2 = biosample data
        for index, xml in xml_test.iterrows():
            action2 = ET.SubElement(submission, 'Action')
            addData = ET.SubElement(action2, 'AddData')
            addData.set('target_db',"BioSample")
            data_tag = ET.SubElement(addData, 'Data')
            data_tag.set('content_type',"xml")
            xmlC = ET.SubElement(data_tag, 'XmlContent')
            bioS = ET.SubElement(xmlC, 'BioSample')
            bioS.set('schema_version',"2.0")
            samID = ET.SubElement(bioS, 'SampleId')
            spuid = ET.SubElement(samID, 'SPUID')
            spuid.set('spuid_namespace',"CDC-OAMD")
            spuid.text = xml['contractor_virus_name_ncbi']
            des = ET.SubElement(bioS, 'Descriptor')
            title = ET.SubElement(des, 'Title')
            title.text = 'SARS-CoV-2 Genome sequencing and assembly - AEGIS'
            org = ET.SubElement(bioS, 'Organism')
            orgName = ET.SubElement(org, 'OrganismName')
            orgName.text = "Severe acute respiratory syndrome coronavirus 2"
            biop = ET.SubElement(bioS, 'BioProject')
            primeID = ET.SubElement(biop, 'PrimaryId')
            primeID.set('db',"BioProject")
            primeID.text = 'PRJNA716984'
            pack = ET.SubElement(bioS, 'Package')
            pack.text = 'SARS-CoV-2.cl.1.0'
            attr = ET.SubElement(bioS,'Attributes')
            attr_str = ET.SubElement(attr, 'Attribute')
            attr_str.set('attribute_name', "Isolate")
            attr_str.text = xml['contractor_virus_name_ncbi']
            attr_by = ET.SubElement(attr, 'Attribute')
            attr_by.set('attribute_name', "collected_by")
            attr_by.text = xml["contractor_vendor_name"]
            attr_date = ET.SubElement(attr, 'Attribute')
            attr_date.set('attribute_name', "collection_date")
            attr_date.text = xml["contractor_collection_date"]
            attr_iosSource = ET.SubElement(attr, 'Attribute')
            attr_iosSource.set('attribute_name', "isolation_source")
            attr_iosSource.text = xml["contractor_specimen"].split(" ")[0]
            attr_geo = ET.SubElement(attr, 'Attribute')
            attr_geo.set('attribute_name', "geo_loc_name")
            attr_geo.text = xml['contractor_country_only']+':'+xml['contractor_stateFull']
            attr_lon_lat = ET.SubElement(attr, 'Attribute')
            attr_lon_lat.set('attribute_name', "lat_lon")
            attr_lon_lat.text = 'missing'
            attr_host = ET.SubElement(attr, 'Attribute')
            attr_host.set('attribute_name', "host")
            attr_host.text = 'Homo-Sapiens'#xml["contractor_host"]
            attr_hostD = ET.SubElement(attr, 'Attribute')
            attr_hostD.set('attribute_name', "host_disease")
            attr_hostD.text = "COVID-19"
            attr_hostS = ET.SubElement(attr, 'Attribute')
            attr_hostS.set('attribute_name', "host_sex")
            attr_hostS.text = xml["contractor_sex"]
            attr_hostA = ET.SubElement(attr, 'Attribute')
            attr_hostA.set('attribute_name', "host_age")
            attr_hostA.text = xml["contractor_age"]
            attr_hostR = ET.SubElement(attr, 'Attribute')
            attr_hostR.set('attribute_name', ",host_race")
            attr_hostR.text = xml["contractor_race"]
            attr_hostAP = ET.SubElement(attr, 'Attribute')
            attr_hostAP.set('attribute_name', "host_anatomical_part")
            attr_hostAP.text = xml["contractor_specimen"]
            attr_colD = ET.SubElement(attr, 'Attribute')
            attr_colD.set('attribute_name', "collection_device")
            attr_colD.text = "Swab" #xml["contractor_specimen"].split(" ")[1]
            attr_colM = ET.SubElement(attr, 'Attribute')
            attr_colM.set('attribute_name', "collection_method")
            attr_colM.text = "Swab" #xml["contractor_specimen"].split(" ")[1]
            attr_diaG = ET.SubElement(attr, 'Attribute')
            attr_diaG.set('attribute_name', "diagnostic_gene_name_1")
            attr_diaG.text = "s gene"
            attr_diaP = ET.SubElement(attr, 'Attribute')
            attr_diaP.set('attribute_name', "diagnostic_pcr_protocol_1")
            attr_diaP.text = xml["PCR_assay"]
            attr_diaCt = ET.SubElement(attr, 'Attribute')
            attr_diaCt.set('attribute_name', "diagnostic_pcr_Ct_value_1")
            attr_diaCt.text = xml["Diagnostic PCR Ct value"]
            attr_diaG = ET.SubElement(attr, 'Attribute')
            attr_diaG.set('attribute_name', "diagnostic_gene_name_2")
            attr_diaG.text = "N2"
            attr_diaP = ET.SubElement(attr, 'Attribute')
            attr_diaP.set('attribute_name', "diagnostic_pcr_protocol_2")
            attr_diaP.text = xml["PCR_assay"]
            attr_diaCt = ET.SubElement(attr, 'Attribute')
            attr_diaCt.set('attribute_name', "diagnostic_pcr_Ct_value_2")
            attr_diaCt.text = xml["N2"]
            attr_diaG = ET.SubElement(attr, 'Attribute')
            attr_diaG.set('attribute_name', "diagnostic_gene_name_3")
            attr_diaG.text = "RP"
            attr_diaP = ET.SubElement(attr, 'Attribute')
            attr_diaP.set('attribute_name', "diagnostic_pcr_protocol_3")
            attr_diaP.text = xml["PCR_assay"]
            attr_diaCt = ET.SubElement(attr, 'Attribute')
            attr_diaCt.set('attribute_name', "diagnostic_pcr_Ct_value_3")
            attr_diaCt.text = xml["RP"]
            attr_clade = ET.SubElement(attr, 'Attribute')
            attr_clade.set('attribute_name', "lineage/clade name")
            attr_clade.text = xml["contractor_lineageCDCgen"]
            id = ET.SubElement(addData, 'Identifier')
            spuid = ET.SubElement(id, 'SPUID')
            spuid.set('spuid_namespace',"CDC-OAMD")
            spuid.text = xml['contractor_virus_name_ncbi']


        for index, xml in xml_test.iterrows():
            action3 = ET.SubElement(submission, 'Action')
            addfile = ET.SubElement(action3, 'AddFiles')
            addfile.set('target_db',"SRA")
            if self.ftype == 'fastq':
                file = ET.SubElement(addfile, 'File')
                file.set('cloud_url', xml['filename'])
                datatype=ET.SubElement(file, "DataType")
                datatype.text = 'generic-data'
                file = ET.SubElement(addfile, 'File')
                file.set('cloud_url', xml['filename2'])
                datatype=ET.SubElement(file, "DataType")
                datatype.text = self.ftype
            else:
                file = ET.SubElement(addfile, 'File')
                file.set('cloud_url', xml['filename'])
                datatype=ET.SubElement(file, "DataType")
                datatype.text = 'generic-data'
                Attr_assem = ET.SubElement(addfile, 'Attribute')
                Attr_assem.set('name',"assembly")
                Attr_assem.text = "NC_045512.2"
            datatype=ET.SubElement(file, "DataType")
            datatype.text = 'generic-data'
            Attr_model = ET.SubElement(addfile, 'Attribute')
            Attr_model.set('name',"instrument_model")
            Attr_model.text = xml['contractor_sequence_machine']
            Attr_name = ET.SubElement(addfile, 'Attribute')
            Attr_name.set('name',"library_name")
            Attr_name.text = xml['contractor_virus_name_ncbi']
            Attr_strat = ET.SubElement(addfile, 'Attribute')
            Attr_strat.set('name',"library_strategy")
            Attr_strat.text = xml['sampling_strategy']
            Attr_source = ET.SubElement(addfile, 'Attribute')
            Attr_source.set('name',"library_source")
            Attr_source.text = 'VIRAL RNA'
            Attr_select = ET.SubElement(addfile, 'Attribute')
            Attr_select.set('name',"library_selection")
            Attr_select.text = 'RT-PCR'
            Attr_lay = ET.SubElement(addfile, 'Attribute')
            Attr_lay.set('name',"library_layout")
            Attr_lay.text = 'PAIRED'
            Attr_prot = ET.SubElement(addfile, 'Attribute')
            Attr_prot.set('name',"library_construction_protocol")
            Attr_prot.text = xml['sequencing protocol name']
            
            Attr_ref = ET.SubElement(addfile, 'AttributeRefId')
            Attr_ref.set("name","BioProject")
            refid = ET.SubElement(Attr_ref, "RefId")
            primid = ET.SubElement(refid, 'PrimaryId')
            primid.text = "PRJNA716984"
            Attr_ref = ET.SubElement(addfile, 'AttributeRefId')
            Attr_ref.set("name","BioSample")
            refid = ET.SubElement(Attr_ref, "RefId")
            spuid = ET.SubElement(refid, 'SPUID')
            spuid.set('spuid_namespace',"CDC-OAMD")
            spuid.text = xml['contractor_virus_name_ncbi']
            id = ET.SubElement(addfile, 'Identifier')
            spuid = ET.SubElement(id, 'SPUID')
            spuid.set('spuid_namespace',"CDC-OAMD")
            spuid.text = xml['contractor_vendor_id']

        return(ET.tostring(submission, pretty_print=True).decode())   

################################################################################
       ###    #####   #######   ##  ###### 
      ## ##   ##     ###    ##  ## ###   ## 
     ##  ##   ####  ###         ##  ###
    ########  ####  ##    ####  ##    ###   
    ###  ###  ##    ###    ###  ## ##   ### 
   ###    ##  #####  #######    ##  ###### 
################################################################################

class ncbi_aegis_metadata:
    """
    Function to extract and transform biosample and metadata to xml format using hadoop, metadata and s3 bucket location files. 
    
    args:
        hadoop_file: The hadoop csv containing data from the runs
        metadata: The metadata file generated from the runs
        loc_file: The bam_file.txt or fastq_files.txt location file generated from running generate_files.sh
        ftype: if the sequencing files are `bam` or `fastq`
        mtype: if the metadata file is `csv` or `tsv`
    """
    def __init__(self, hadoop_file, metadata, loc_file='./../sars-cov-2-upload/bam_files.txt',ftype='bam',mtype='tsv'):
        self.hadoop = hadoop_file
        self.metadata = metadata
        self.files = loc_file
        self.ftype = ftype
        self.mtype = mtype

    def read(self):
        """
        Combineds hadoop and metadata files into a pandas dataframe and adds a column with the s3 bucket file locations.
        """
        hadoop =  pd.read_csv(self.hadoop, dtype='string')
        if self.mtype == 'csv':
            metadata = pd.read_csv(self.metadata, dtype='string')
        elif self.mtype == 'tsv':
            metadata = pd.read_csv(self.metadata, dtype='string', sep='\t')
        aegis = hadoop.join(metadata.set_index('sample_name'), on='contractor_sample_name_old')
        if self.ftype == 'fastq':
            file_loc = pd.read_csv(self.files, header=None)
            file_loc.rename({0:"loc"}, axis=1, inplace=True)
            file_loc = file_loc[file_loc['loc'].str.contains('quest')]
            file_loc["pre"] = "s3://ncezid-non-pii"
            file_loc["seq_barcode"] = file_loc["loc"].str.split("/", expand=True)[7].str.split("_",expand=True)[0]
            file_loc["seq_barcode"] = file_loc["seq_barcode"].str.split("_",expand=True)[0]
            file_loc["seq_barcode"] = file_loc["seq_barcode"].str.split("-",expand=True)[2]
            file_loc['loc'] = file_loc['pre'].str.cat(file_loc['loc'],sep="/")
            file_loc.drop(['pre'], axis=1, inplace=True)
            locs = file_loc.groupby('seq_barcode').agg(lambda x: x.tolist())
            locs.reset_index(inplace=True)
            locs[['filename','filename2']] = pd.DataFrame(locs['loc'].tolist(), index= locs.index)
        elif self.ftype == 'bam':
            file_loc = pd.read_csv(self.files, header=None)
            file_loc.rename({0:"filename"}, axis=1, inplace=True)
            file_loc = file_loc[file_loc['filename'].str.contains('aegis')]
            file_loc["pre"] = "s3://ncezid-non-pii"
            file_loc["seq_barcode"] = file_loc["filename"].str.split("/", expand=True)[6].str.split('-',expand=True)[0]
            file_loc['filename'] = file_loc['pre'].str.cat(file_loc['filename'],sep="/")
            file_loc.drop(['pre'], axis=1, inplace=True)
            locs = file_loc
        aegis.fillna('missing', inplace=True)
        aegis = aegis.join(locs.set_index('seq_barcode'), on='contractor_vendor_id',how='left')
        aegis['contractor_country_only'] = aegis['contractor_country_only'].str.replace("united States", "USA", case=False)
        return aegis
    
#    today = date.today()
#    today = today.strftime("%d-%m-%Y")

# create the file structure
    def cov_2_xml(self):
        """
        Writes xml file containing biosample data and metadata to standard out. BioProject is hard coded.
        """
        day = str(date.today()) #'12/Oct/2013'
        dt = datetime.strptime(day, '%Y-%m-%d')
        today = dt + timedelta(days=14)
        release = today.strftime("%Y-%m-%d")
        xml_test = ncbi_aegis_metadata.read(self)
        submission = ET.Element('Submission')
        description = ET.SubElement(submission, 'Description')
        title = ET.SubElement(description, 'Title')
        title.text = 'SARS-CoV-2 Genome sequencing and assembly - AEGIS'+"_".join(self.hadoop.split('/')[11].split('_')[0:2])
        comment = ET.SubElement(description, 'Comment')
        comment.text = 'BP(1.0)+BS(1.0)+SRA'
        org = ET.SubElement(description, 'Organization')
        org.set('role', 'owner')
        org.set('type','institute')
        org.set('org_id', "20768")
        name = ET.SubElement(org, 'Name')
        email = ET.SubElement(org, 'Contact')
        email.set('email', 'qdy1@cdc.gov')
        name.text = 'Centers for Disease Control and Prevention'
        name = ET.SubElement(email, 'Name')
        first = ET.SubElement(name, 'First')
        last = ET.SubElement(name, 'Last')
        first.text = "Elijah"
        last.text = "Lowe"
        hold = ET.SubElement(description, 'Hold')
        hold.set('release_date', str(release))
        
		
        #Action block 2 = biosample data
        for index, xml in xml_test.iterrows():
            action2 = ET.SubElement(submission, 'Action')
            addData = ET.SubElement(action2, 'AddData')
            addData.set('target_db',"BioSample")
            data_tag = ET.SubElement(addData, 'Data')
            data_tag.set('content_type',"xml")
            xmlC = ET.SubElement(data_tag, 'XmlContent')
            bioS = ET.SubElement(xmlC, 'BioSample')
            bioS.set('schema_version',"2.0")
            samID = ET.SubElement(bioS, 'SampleId')
            spuid = ET.SubElement(samID, 'SPUID')
            spuid.set('spuid_namespace',"CDC-OAMD")
            spuid.text = xml['contractor_virus_name_ncbi']
            des = ET.SubElement(bioS, 'Descriptor')
            title = ET.SubElement(des, 'Title')
            title.text = 'SARS-CoV-2 Genome sequencing and assembly - AEGIS'
            org = ET.SubElement(bioS, 'Organism')
            orgName = ET.SubElement(org, 'OrganismName')
            orgName.text = "Severe acute respiratory syndrome coronavirus 2"
            biop = ET.SubElement(bioS, 'BioProject')
            primeID = ET.SubElement(biop, 'PrimaryId')
            primeID.set('db',"BioProject")
            primeID.text = 'PRJNA731148'
            pack = ET.SubElement(bioS, 'Package')
            pack.text = 'SARS-CoV-2.cl.1.0'
            attr = ET.SubElement(bioS,'Attributes')
            attr_str = ET.SubElement(attr, 'Attribute')
            attr_str.set('attribute_name', "Isolate")
            attr_str.text = xml['contractor_virus_name_ncbi']
            attr_by = ET.SubElement(attr, 'Attribute')
            attr_by.set('attribute_name', "collected_by")
            attr_by.text = xml["contractor_vendor_name"]
            attr_date = ET.SubElement(attr, 'Attribute')
            attr_date.set('attribute_name', "collection_date")
            attr_date.text = xml["contractor_collection_date"]
            attr_iosSource = ET.SubElement(attr, 'Attribute')
            attr_iosSource.set('attribute_name', "isolation_source")
            attr_iosSource.text = xml["contractor_specimen"].split(" ")[0]
            attr_geo = ET.SubElement(attr, 'Attribute')
            attr_geo.set('attribute_name', "geo_loc_name")
            attr_geo.text = xml['contractor_country_only']+':'+xml['contractor_stateFull']
            attr_lon_lat = ET.SubElement(attr, 'Attribute')
            attr_lon_lat.set('attribute_name', "lat_lon")
            attr_lon_lat.text = 'missing'
            attr_host = ET.SubElement(attr, 'Attribute')
            attr_host.set('attribute_name', "host")
            attr_host.text = 'Homo-Sapiens'#xml["contractor_host"]
            attr_hostD = ET.SubElement(attr, 'Attribute')
            attr_hostD.set('attribute_name', "host_disease")
            attr_hostD.text = "COVID-19"
            attr_hostS = ET.SubElement(attr, 'Attribute')
            attr_hostS.set('attribute_name', "host_sex")
            attr_hostS.text = xml["contractor_sex"]
            attr_hostA = ET.SubElement(attr, 'Attribute')
            attr_hostA.set('attribute_name', "host_age")
            attr_hostA.text = xml["contractor_age"]
            attr_hostR = ET.SubElement(attr, 'Attribute')
            attr_hostR.set('attribute_name', ",host_race")
            attr_hostR.text = xml["contractor_race"]
            attr_hostAP = ET.SubElement(attr, 'Attribute')
            attr_hostAP.set('attribute_name', "host_anatomical_part")
            attr_hostAP.text = xml["contractor_specimen"]
            attr_colD = ET.SubElement(attr, 'Attribute')
            attr_colD.set('attribute_name', "collection_device")
            attr_colD.text = "Swab" #xml["contractor_specimen"].split(" ")[1]
            attr_colM = ET.SubElement(attr, 'Attribute')
            attr_colM.set('attribute_name', "collection_method")
            attr_colM.text = "Swab" #xml["contractor_specimen"].split(" ")[1]
            attr_diaG = ET.SubElement(attr, 'Attribute')
            attr_diaG.set('attribute_name', "diagnostic_gene_name_1")
            attr_diaG.text = "n gene"
            attr_diaP = ET.SubElement(attr, 'Attribute')
            attr_diaP.set('attribute_name', "diagnostic_pcr_protocol_1")
            attr_diaP.text = xml["PCR_assay"]
            attr_diaCt = ET.SubElement(attr, 'Attribute')
            attr_diaCt.set('attribute_name', "diagnostic_pcr_Ct_value_1")
            attr_diaCt.text = xml["Ct_N"]
            attr_diaG = ET.SubElement(attr, 'Attribute')
            attr_diaG.set('attribute_name', "diagnostic_gene_name_2")
            attr_diaG.text = "S gene"
            attr_diaP = ET.SubElement(attr, 'Attribute')
            attr_diaP.set('attribute_name', "diagnostic_pcr_protocol_2")
            attr_diaP.text = xml["PCR_assay"]
            attr_diaCt = ET.SubElement(attr, 'Attribute')
            attr_diaCt.set('attribute_name', "diagnostic_pcr_Ct_value_2")
            attr_diaCt.text = xml["Ct_S"]
            attr_diaG = ET.SubElement(attr, 'Attribute')
            attr_diaG.set('attribute_name', "diagnostic_gene_name_3")
            attr_diaG.text = "ORF1ab"
            attr_diaP = ET.SubElement(attr, 'Attribute')
            attr_diaP.set('attribute_name', "diagnostic_pcr_protocol_3")
            attr_diaP.text = xml["PCR_assay"]
            attr_diaCt = ET.SubElement(attr, 'Attribute')
            attr_diaCt.set('attribute_name', "diagnostic_pcr_Ct_value_3")
            attr_diaCt.text = xml["Ct_ORF1ab"]
            attr_clade = ET.SubElement(attr, 'Attribute')
            attr_clade.set('attribute_name', "lineage/clade name")
            attr_clade.text = xml["contractor_lineageCDCgen"]
            id = ET.SubElement(addData, 'Identifier')
            spuid = ET.SubElement(id, 'SPUID')
            spuid.set('spuid_namespace',"CDC-OAMD")
            spuid.text = xml['contractor_virus_name_ncbi']


        for index, xml in xml_test.iterrows():
            action3 = ET.SubElement(submission, 'Action')
            addfile = ET.SubElement(action3, 'AddFiles')
            addfile.set('target_db',"SRA")
            if self.ftype == 'fastq':
                file = ET.SubElement(addfile, 'File')
                file.set('cloud_url', xml['filename'])
                datatype=ET.SubElement(file, "DataType")
                datatype.text = 'generic-data'
                file = ET.SubElement(addfile, 'File')
                file.set('cloud_url', xml['filename2'])
                datatype=ET.SubElement(file, "DataType")
                datatype.text = self.ftype
            else:
                file = ET.SubElement(addfile, 'File')
                file.set('cloud_url', xml['filename'])
                datatype=ET.SubElement(file, "DataType")
                datatype.text = 'generic-data'
                Attr_assem = ET.SubElement(addfile, 'Attribute')
                Attr_assem.set('name',"assembly")
                Attr_assem.text = "NC_045512.2"
            datatype=ET.SubElement(file, "DataType")
            datatype.text = 'generic-data'
            Attr_model = ET.SubElement(addfile, 'Attribute')
            Attr_model.set('name',"instrument_model")
            Attr_model.text = xml['contractor_sequence_machine']
            Attr_name = ET.SubElement(addfile, 'Attribute')
            Attr_name.set('name',"library_name")
            Attr_name.text = xml['contractor_virus_name_ncbi']
            Attr_strat = ET.SubElement(addfile, 'Attribute')
            Attr_strat.set('name',"library_strategy")
            Attr_strat.text = xml['sampling_strategy']
            Attr_source = ET.SubElement(addfile, 'Attribute')
            Attr_source.set('name',"library_source")
            Attr_source.text = 'VIRAL RNA'
            Attr_select = ET.SubElement(addfile, 'Attribute')
            Attr_select.set('name',"library_selection")
            Attr_select.text = 'RT-PCR'
            Attr_lay = ET.SubElement(addfile, 'Attribute')
            Attr_lay.set('name',"library_layout")
            Attr_lay.text = 'PAIRED'
            Attr_prot = ET.SubElement(addfile, 'Attribute')
            Attr_prot.set('name',"library_construction_protocol")
            Attr_prot.text = xml['sequencing protocol name']
            Attr_ref = ET.SubElement(addfile, 'AttributeRefId')
            Attr_ref.set("name","BioProject")
            refid = ET.SubElement(Attr_ref, "RefId")
            primid = ET.SubElement(refid, 'PrimaryId')
            primid.text = "PRJNA731148"
            Attr_ref = ET.SubElement(addfile, 'AttributeRefId')
            Attr_ref.set("name","BioSample")
            refid = ET.SubElement(Attr_ref, "RefId")
            spuid = ET.SubElement(refid, 'SPUID')
            spuid.set('spuid_namespace',"CDC-OAMD")
            spuid.text = xml['contractor_virus_name_ncbi']
            id = ET.SubElement(addfile, 'Identifier')
            spuid = ET.SubElement(id, 'SPUID')
            spuid.set('spuid_namespace',"CDC-OAMD")
            spuid.text = xml['contractor_vendor_id']

        return(ET.tostring(submission, pretty_print=True).decode())

    
###########################################################################
       ######## ###  ### ###       ######### ######### ###   ### #########
       ####     ###  ### ###       ######### ####      ####  ### #########
       ######## ###  ### ###       ######### #######   ### #####   ####
       ####     ######## ######### ######### ####      ###  ####   ####
       ####     ######## ######### ######### ######### ###   ###   ####
###########################################################################

class ncbi_fulgent_metadata:
    """
    Function to extract and transform biosample and metadata to xml format using hadoop, metadata and s3 bucket location files. 
    
    args:
        hadoop_file: The hadoop csv containing data from the runs
        metadata: The metadata file generated from the runs
        loc_file: The bam_file.txt or fastq_files.txt location file generated from running generate_files.sh
        ftype: if the sequencing files are `bam` or `fastq`
        mtype: if the metadata file is `csv` or `tsv`
    """
    def __init__(self, hadoop_file, metadata, loc_file='./../sars-cov-2-upload/bam_files.txt',ftype='bam',mtype='tsv'):
        self.hadoop = hadoop_file
        self.metadata = metadata
        self.files = loc_file
        self.ftype = ftype
        self.mtype = mtype

    def read(self):
        """
        Combineds hadoop and metadata files into a pandas dataframe and adds a column with the s3 bucket file locations.
        """
        hadoop =  pd.read_csv(self.hadoop, dtype='string')
        if self.mtype == 'csv':
            metadata = pd.read_csv(self.metadata, dtype='string')
        elif self.mtype == 'tsv':
            metadata = pd.read_csv(self.metadata, dtype='string', sep='\t')
        fulgent = hadoop.join(metadata.set_index('sample_name'), on='contractor_sample_name_old')
        if self.ftype == 'fastq':
            file_loc = pd.read_csv(self.files, header=None)
            file_loc.rename({0:"loc"}, axis=1, inplace=True)
            file_loc = file_loc[file_loc['loc'].str.contains('quest')]
            file_loc["pre"] = "s3://ncezid-non-pii"
            file_loc["seq_barcode"] = file_loc["loc"].str.split("/", expand=True)[7].str.split("_",expand=True)[0]
            file_loc["seq_barcode"] = file_loc["seq_barcode"].str.split("_",expand=True)[0]
            file_loc["seq_barcode"] = file_loc["seq_barcode"].str.split("-",expand=True)[2]
            file_loc['loc'] = file_loc['pre'].str.cat(file_loc['loc'],sep="/")
            file_loc.drop(['pre'], axis=1, inplace=True)
            locs = file_loc.groupby('seq_barcode').agg(lambda x: x.tolist())
            locs.reset_index(inplace=True)
            locs[['filename','filename2']] = pd.DataFrame(locs['loc'].tolist(), index= locs.index)
        elif self.ftype == 'bam':
            file_loc = pd.read_csv(self.files, header=None)
            file_loc.rename({0:"filename"}, axis=1, inplace=True)
            file_loc = file_loc[file_loc['filename'].str.contains('fulgent')]
            file_loc["pre"] = "s3://ncezid-non-pii"
            file_loc["seq_barcode"] = file_loc["filename"].str.split("/", expand=True)[7].str.split('_',expand=True)[0]
            file_loc['filename'] = file_loc['pre'].str.cat(file_loc['filename'],sep="/")
            file_loc.drop(['pre'], axis=1, inplace=True)
            locs = file_loc
        fulgent.fillna('missing', inplace=True)
        fulgent = fulgent.join(locs.set_index('seq_barcode'), on='contractor_vendor_id',how='left')
        fulgent['contractor_country_only'] = fulgent['contractor_country_only'].str.replace("united States", "USA", case=False)
        return fulgent
    

# create the file structure
    def cov_2_xml(self):
        """
        Writes xml file containing biosample data and metadata to standard out. BioProject is hard coded.
        """
        day = str(date.today()) #'12/Oct/2013'
        dt = datetime.strptime(day, '%Y-%m-%d')
        today = dt + timedelta(days=14)
        release = today.strftime("%Y-%m-%d")
        xml_test = ncbi_fulgent_metadata.read(self)
        submission = ET.Element('Submission')
        description = ET.SubElement(submission, 'Description')
        title = ET.SubElement(description, 'Title')
        title.text = 'SARS-CoV-2 Genome sequencing and assembly - fulgent'+"_".join(self.hadoop.split('/')[11].split('_')[0:2])
        comment = ET.SubElement(description, 'Comment')
        comment.text = 'BP(1.0)+BS(1.0)+SRA'
        org = ET.SubElement(description, 'Organization')
        org.set('role', 'owner')
        org.set('type','institute')
        org.set('org_id', "20768")
        name = ET.SubElement(org, 'Name')
        email = ET.SubElement(org, 'Contact')
        email.set('email', 'qdy1@cdc.gov')
        name.text = 'Centers for Disease Control and Prevention'
        name = ET.SubElement(email, 'Name')
        first = ET.SubElement(name, 'First')
        last = ET.SubElement(name, 'Last')
        first.text = "Elijah"
        last.text = "Lowe"
        hold = ET.SubElement(description, 'Hold')
        hold.set('release_date', str(release))
        
		
        #Action block 2 = biosample data
        for index, xml in xml_test.iterrows():
            action2 = ET.SubElement(submission, 'Action')
            addData = ET.SubElement(action2, 'AddData')
            addData.set('target_db',"BioSample")
            data_tag = ET.SubElement(addData, 'Data')
            data_tag.set('content_type',"xml")
            xmlC = ET.SubElement(data_tag, 'XmlContent')
            bioS = ET.SubElement(xmlC, 'BioSample')
            bioS.set('schema_version',"2.0")
            samID = ET.SubElement(bioS, 'SampleId')
            spuid = ET.SubElement(samID, 'SPUID')
            spuid.set('spuid_namespace',"CDC-OAMD")
            spuid.text = xml['contractor_virus_name_ncbi']
            des = ET.SubElement(bioS, 'Descriptor')
            title = ET.SubElement(des, 'Title')
            title.text = 'SARS-CoV-2 Genome sequencing and assembly - fulgent'
            org = ET.SubElement(bioS, 'Organism')
            orgName = ET.SubElement(org, 'OrganismName')
            orgName.text = "Severe acute respiratory syndrome coronavirus 2"
            biop = ET.SubElement(bioS, 'BioProject')
            primeID = ET.SubElement(biop, 'PrimaryId')
            primeID.set('db',"BioProject")
            primeID.text = 'PRJNA731152'
            pack = ET.SubElement(bioS, 'Package')
            pack.text = 'SARS-CoV-2.cl.1.0'
            attr = ET.SubElement(bioS,'Attributes')
            attr_str = ET.SubElement(attr, 'Attribute')
            attr_str.set('attribute_name', "Isolate")
            attr_str.text = xml['contractor_virus_name_ncbi']
            attr_by = ET.SubElement(attr, 'Attribute')
            attr_by.set('attribute_name', "collected_by")
            attr_by.text = xml["contractor_vendor_name"]
            attr_date = ET.SubElement(attr, 'Attribute')
            attr_date.set('attribute_name', "collection_date")
            attr_date.text = xml["contractor_collection_date"]
            attr_iosSource = ET.SubElement(attr, 'Attribute')
            attr_iosSource.set('attribute_name', "isolation_source")
            attr_iosSource.text = xml["contractor_specimen"].split(" ")[0]
            attr_geo = ET.SubElement(attr, 'Attribute')
            attr_geo.set('attribute_name', "geo_loc_name")
            attr_geo.text = xml['contractor_country_only']+':'+xml['contractor_stateFull']
            attr_lon_lat = ET.SubElement(attr, 'Attribute')
            attr_lon_lat.set('attribute_name', "lat_lon")
            attr_lon_lat.text = 'missing'
            attr_host = ET.SubElement(attr, 'Attribute')
            attr_host.set('attribute_name', "host")
            attr_host.text = 'Homo-Sapiens'#xml["contractor_host"]
            attr_hostD = ET.SubElement(attr, 'Attribute')
            attr_hostD.set('attribute_name', "host_disease")
            attr_hostD.text = "COVID-19"
            attr_hostS = ET.SubElement(attr, 'Attribute')
            attr_hostS.set('attribute_name', "host_sex")
            attr_hostS.text = xml["contractor_sex"]
            attr_hostA = ET.SubElement(attr, 'Attribute')
            attr_hostA.set('attribute_name', "host_age")
            attr_hostA.text = xml["contractor_age"]
            attr_hostR = ET.SubElement(attr, 'Attribute')
            attr_hostR.set('attribute_name', ",host_race")
            attr_hostR.text = xml["contractor_race"]
            attr_hostAP = ET.SubElement(attr, 'Attribute')
            attr_hostAP.set('attribute_name', "host_anatomical_part")
            attr_hostAP.text = xml["contractor_specimen"]
            attr_colD = ET.SubElement(attr, 'Attribute')
            attr_colD.set('attribute_name', "collection_device")
            attr_colD.text = "Swab" #xml["contractor_specimen"].split(" ")[1]
            attr_colM = ET.SubElement(attr, 'Attribute')
            attr_colM.set('attribute_name', "collection_method")
            attr_colM.text = "Swab" #xml["contractor_specimen"].split(" ")[1]
            attr_diaG = ET.SubElement(attr, 'Attribute')
            attr_diaG.set('attribute_name', "diagnostic_gene_name_1")
            attr_diaG.text = "SC2"
            attr_diaP = ET.SubElement(attr, 'Attribute')
            attr_diaP.set('attribute_name', "diagnostic_pcr_protocol_1")
            attr_diaP.text = xml["PCR_assay"]
            attr_diaCt = ET.SubElement(attr, 'Attribute')
            attr_diaCt.set('attribute_name', "diagnostic_pcr_Ct_value_1")
            attr_diaCt.text = xml["Ct_SC2"]
            attr_diaG = ET.SubElement(attr, 'Attribute')
            attr_diaG.set('attribute_name', "diagnostic_gene_name_2")
            attr_diaG.text = "N1"
            attr_diaP = ET.SubElement(attr, 'Attribute')
            attr_diaP.set('attribute_name', "diagnostic_pcr_protocol_2")
            attr_diaP.text = xml["PCR_assay"]
            attr_diaCt = ET.SubElement(attr, 'Attribute')
            attr_diaCt.set('attribute_name', "diagnostic_pcr_Ct_value_2")
            attr_diaCt.text = xml["Ct_N1"]
            attr_diaG = ET.SubElement(attr, 'Attribute')
            attr_diaG.set('attribute_name', "diagnostic_gene_name_3")
            attr_diaG.text = "N2"
            attr_diaP = ET.SubElement(attr, 'Attribute')
            attr_diaP.set('attribute_name', "diagnostic_pcr_protocol_3")
            attr_diaP.text = xml["PCR_assay"]
            attr_diaCt = ET.SubElement(attr, 'Attribute')
            attr_diaCt.set('attribute_name', "diagnostic_pcr_Ct_value_3")
            attr_diaCt.text = xml["Ct_N2"]
            attr_clade = ET.SubElement(attr, 'Attribute')
            attr_clade.set('attribute_name', "lineage/clade name")
            attr_clade.text = xml["contractor_lineageCDCgen"]
            id = ET.SubElement(addData, 'Identifier')
            spuid = ET.SubElement(id, 'SPUID')
            spuid.set('spuid_namespace',"CDC-OAMD")
            spuid.text = xml['contractor_virus_name_ncbi']


        for index, xml in xml_test.iterrows():
            action3 = ET.SubElement(submission, 'Action')
            addfile = ET.SubElement(action3, 'AddFiles')
            addfile.set('target_db',"SRA")
            if self.ftype == 'fastq':
                file = ET.SubElement(addfile, 'File')
                file.set('cloud_url', xml['filename'])
                datatype=ET.SubElement(file, "DataType")
                datatype.text = 'generic-data'
                file = ET.SubElement(addfile, 'File')
                file.set('cloud_url', xml['filename2'])
                datatype=ET.SubElement(file, "DataType")
                datatype.text = self.ftype
            else:
                file = ET.SubElement(addfile, 'File')
                file.set('cloud_url', xml['filename'])
                datatype=ET.SubElement(file, "DataType")
                datatype.text = 'generic-data'
                Attr_assem = ET.SubElement(addfile, 'Attribute')
                Attr_assem.set('name',"assembly")
                Attr_assem.text = "NC_045512.2"
            datatype=ET.SubElement(file, "DataType")
            datatype.text = 'generic-data'
            Attr_model = ET.SubElement(addfile, 'Attribute')
            Attr_model.set('name',"instrument_model")
            Attr_model.text = xml['contractor_sequence_machine']
            Attr_name = ET.SubElement(addfile, 'Attribute')
            Attr_name.set('name',"library_name")
            Attr_name.text = xml['contractor_virus_name_ncbi']
            Attr_strat = ET.SubElement(addfile, 'Attribute')
            Attr_strat.set('name',"library_strategy")
            Attr_strat.text = xml['sampling_strategy']
            Attr_source = ET.SubElement(addfile, 'Attribute')
            Attr_source.set('name',"library_source")
            Attr_source.text = 'VIRAL RNA'
            Attr_select = ET.SubElement(addfile, 'Attribute')
            Attr_select.set('name',"library_selection")
            Attr_select.text = 'RT-PCR'
            Attr_lay = ET.SubElement(addfile, 'Attribute')
            Attr_lay.set('name',"library_layout")
            Attr_lay.text = 'PAIRED'
            Attr_prot = ET.SubElement(addfile, 'Attribute')
            Attr_prot.set('name',"library_construction_protocol")
            Attr_prot.text = xml['sampling_strategy']
            Attr_ref = ET.SubElement(addfile, 'AttributeRefId')
            Attr_ref.set("name","BioProject")
            refid = ET.SubElement(Attr_ref, "RefId")
            primid = ET.SubElement(refid, 'PrimaryId')
            primid.text = "PRJNA731152"
            Attr_ref = ET.SubElement(addfile, 'AttributeRefId')
            Attr_ref.set("name","BioSample")
            refid = ET.SubElement(Attr_ref, "RefId")
            spuid = ET.SubElement(refid, 'SPUID')
            spuid.set('spuid_namespace',"CDC-OAMD")
            spuid.text = xml['contractor_virus_name_ncbi']
            id = ET.SubElement(addfile, 'Identifier')
            spuid = ET.SubElement(id, 'SPUID')
            spuid.set('spuid_namespace',"CDC-OAMD")
            spuid.text = xml['contractor_vendor_id']

        return(ET.tostring(submission, pretty_print=True).decode())


###########################################################################
#####   ###      ### ##########  ##### ###       ### ##### ######### ###     ###
#####   ####     ### ##########  ##### ####      ### ##### #########  ###   ###
 ###    #######  ### ####         ###  #######   ###  ###     ###      ### ###
 ###    ############ #######      ###  #############  ###     ###        ###
 ###    ####   ##### #######      ###  ###   #######  ###     ###        ###
#####   ####    #### ####        ##### ###      #### #####    ###        ###
#####   ####     ### ####        ##### ###       ### #####    ###        ###
###########################################################################

class ncbi_infinity_metadata:
    """
    Function to extract and transform biosample and metadata to xml format using hadoop, metadata and s3 bucket location files. 
    
    args:
        hadoop_file: The hadoop csv containing data from the runs
        metadata: The metadata file generated from the runs
        loc_file: The bam_file.txt or fastq_files.txt location file generated from running generate_files.sh
        ftype: if the sequencing files are `bam` or `fastq`
        mtype: if the metadata file is `csv` or `tsv`
    """
    def __init__(self, hadoop_file, metadata, loc_file='./../sars-cov-2-upload/bam_files.txt',ftype='bam',mtype='csv'):
        self.hadoop = hadoop_file
        self.metadata = metadata
        self.files = loc_file
        self.ftype = ftype
        self.mtype = mtype

    def read(self):
        """
        Combineds hadoop and metadata files into a pandas dataframe and adds a column with the s3 bucket file locations.
        """
        hadoop =  pd.read_csv(self.hadoop, dtype='string')
        if self.mtype == 'csv':
            metadata = pd.read_csv(self.metadata, dtype='string')
        elif self.mtype == 'tsv':
            metadata = pd.read_csv(self.metadata, dtype='string', sep='\t')
        infinity = hadoop.join(metadata.set_index('sample_name'), on='contractor_sample_name_old')
        if self.ftype == 'fastq':
            file_loc = pd.read_csv(self.files, header=None)
            file_loc.rename({0:"loc"}, axis=1, inplace=True)
            file_loc = file_loc[file_loc['loc'].str.contains('quest')]
            file_loc["pre"] = "s3://ncezid-non-pii"
            file_loc["seq_barcode"] = file_loc["loc"].str.split("/", expand=True)[7].str.split("_",expand=True)[0]
            file_loc["seq_barcode"] = file_loc["seq_barcode"].str.split("_",expand=True)[0]
            file_loc["seq_barcode"] = file_loc["seq_barcode"].str.split("-",expand=True)[2]
            file_loc['loc'] = file_loc['pre'].str.cat(file_loc['loc'],sep="/")
            file_loc.drop(['pre'], axis=1, inplace=True)
            locs = file_loc.groupby('seq_barcode').agg(lambda x: x.tolist())
            locs.reset_index(inplace=True)
            locs[['filename','filename2']] = pd.DataFrame(locs['loc'].tolist(), index= locs.index)
        elif self.ftype == 'bam':
            file_loc = pd.read_csv(self.files, header=None)
            file_loc.rename({0:"filename"}, axis=1, inplace=True)
            file_loc = file_loc[file_loc['filename'].str.contains('infinity')]
            file_loc["pre"] = "s3://ncezid-non-pii"
            file_loc["seq_barcode"] = "IBX_"+file_loc["filename"].str.split("/", expand=True)[7].str.split('_', expand=True)[1]
            file_loc['filename'] = file_loc['pre'].str.cat(file_loc['filename'],sep="/")
            file_loc.drop(['pre'], axis=1, inplace=True)
            locs = file_loc
        infinity.fillna('missing', inplace=True)
        infinity = infinity.join(locs.set_index('seq_barcode'), on='contractor_sample_name_old',how='left')
        infinity['contractor_country_only'] = infinity['contractor_country_only'].str.replace("united States", "USA", case=False)
        return infinity
    

# create the file structure
    def cov_2_xml(self):
        """
        Writes xml file containing biosample data and metadata to standard out. BioProject is hard coded.
        """
        day = str(date.today()) #'12/Oct/2013'
        dt = datetime.strptime(day, '%Y-%m-%d')
        today = dt + timedelta(days=14)
        release = today.strftime("%Y-%m-%d")
        xml_test = ncbi_infinity_metadata.read(self)
        submission = ET.Element('Submission')
        description = ET.SubElement(submission, 'Description')
        title = ET.SubElement(description, 'Title')
        title.text = 'SARS-CoV-2 Genome sequencing and assembly - fulgent'+"_".join(self.hadoop.split('/')[11].split('_')[0:2])
        comment = ET.SubElement(description, 'Comment')
        comment.text = 'BP(1.0)+BS(1.0)+SRA'
        org = ET.SubElement(description, 'Organization')
        org.set('role', 'owner')
        org.set('type','institute')
        org.set('org_id', "20768")
        name = ET.SubElement(org, 'Name')
        email = ET.SubElement(org, 'Contact')
        email.set('email', 'qdy1@cdc.gov')
        name.text = 'Centers for Disease Control and Prevention'
        name = ET.SubElement(email, 'Name')
        first = ET.SubElement(name, 'First')
        last = ET.SubElement(name, 'Last')
        first.text = "Elijah"
        last.text = "Lowe"
        hold = ET.SubElement(description, 'Hold')
        hold.set('release_date', str(release))
        
		
        #Action block 2 = biosample data
        for index, xml in xml_test.iterrows():
            action2 = ET.SubElement(submission, 'Action')
            addData = ET.SubElement(action2, 'AddData')
            addData.set('target_db',"BioSample")
            data_tag = ET.SubElement(addData, 'Data')
            data_tag.set('content_type',"xml")
            xmlC = ET.SubElement(data_tag, 'XmlContent')
            bioS = ET.SubElement(xmlC, 'BioSample')
            bioS.set('schema_version',"2.0")
            samID = ET.SubElement(bioS, 'SampleId')
            spuid = ET.SubElement(samID, 'SPUID')
            spuid.set('spuid_namespace',"CDC-OAMD")
            spuid.text = xml['contractor_virus_name_ncbi']
            des = ET.SubElement(bioS, 'Descriptor')
            title = ET.SubElement(des, 'Title')
            title.text = 'SARS-CoV-2 Genome sequencing and assembly - fulgent'
            org = ET.SubElement(bioS, 'Organism')
            orgName = ET.SubElement(org, 'OrganismName')
            orgName.text = "Severe acute respiratory syndrome coronavirus 2"
            biop = ET.SubElement(bioS, 'BioProject')
            primeID = ET.SubElement(biop, 'PrimaryId')
            primeID.set('db',"BioProject")
            primeID.text = 'PRJNA731149'
            pack = ET.SubElement(bioS, 'Package')
            pack.text = 'SARS-CoV-2.cl.1.0'
            attr = ET.SubElement(bioS,'Attributes')
            attr_str = ET.SubElement(attr, 'Attribute')
            attr_str.set('attribute_name', "Isolate")
            attr_str.text = xml['contractor_virus_name_ncbi']
            attr_by = ET.SubElement(attr, 'Attribute')
            attr_by.set('attribute_name', "collected_by")
            attr_by.text = xml["contractor_vendor_name"]
            attr_date = ET.SubElement(attr, 'Attribute')
            attr_date.set('attribute_name', "collection_date")
            attr_date.text = xml["contractor_collection_date"]
            attr_iosSource = ET.SubElement(attr, 'Attribute')
            attr_iosSource.set('attribute_name', "isolation_source")
            attr_iosSource.text = xml["contractor_specimen"].split(" ")[0]
            attr_geo = ET.SubElement(attr, 'Attribute')
            attr_geo.set('attribute_name', "geo_loc_name")
            attr_geo.text = xml['contractor_country_only']+':'+xml['contractor_stateFull']
            attr_lon_lat = ET.SubElement(attr, 'Attribute')
            attr_lon_lat.set('attribute_name', "lat_lon")
            attr_lon_lat.text = 'missing'
            attr_host = ET.SubElement(attr, 'Attribute')
            attr_host.set('attribute_name', "host")
            attr_host.text = 'Homo-Sapiens'#xml["contractor_host"]
            attr_hostD = ET.SubElement(attr, 'Attribute')
            attr_hostD.set('attribute_name', "host_disease")
            attr_hostD.text = "COVID-19"
            attr_hostS = ET.SubElement(attr, 'Attribute')
            attr_hostS.set('attribute_name', "host_sex")
            attr_hostS.text = xml["contractor_sex"]
            attr_hostA = ET.SubElement(attr, 'Attribute')
            attr_hostA.set('attribute_name', "host_age")
            attr_hostA.text = xml["contractor_age"]
            attr_hostR = ET.SubElement(attr, 'Attribute')
            attr_hostR.set('attribute_name', ",host_race")
            attr_hostR.text = xml["contractor_race"]
            attr_hostAP = ET.SubElement(attr, 'Attribute')
            attr_hostAP.set('attribute_name', "host_anatomical_part")
            attr_hostAP.text = xml["contractor_specimen"]
            attr_colD = ET.SubElement(attr, 'Attribute')
            attr_colD.set('attribute_name', "collection_device")
            attr_colD.text = "Swab" #xml["contractor_specimen"].split(" ")[1]
            attr_colM = ET.SubElement(attr, 'Attribute')
            attr_colM.set('attribute_name', "collection_method")
            attr_colM.text = "Swab" #xml["contractor_specimen"].split(" ")[1]
            attr_diaG = ET.SubElement(attr, 'Attribute')
            attr_diaG.set('attribute_name', "diagnostic_gene_name_1")
            attr_diaG.text = "n gene"
            attr_diaP = ET.SubElement(attr, 'Attribute')
            attr_diaP.set('attribute_name', "diagnostic_pcr_protocol_1")
            attr_diaP.text = xml["PCR_assay"]
            attr_diaCt = ET.SubElement(attr, 'Attribute')
            attr_diaCt.set('attribute_name', "diagnostic_pcr_Ct_value_1")
            attr_diaCt.text = xml["ct_ngene"]
            attr_clade = ET.SubElement(attr, 'Attribute')
            attr_clade.set('attribute_name', "lineage/clade name")
            attr_clade.text = xml["contractor_lineageCDCgen"]
            id = ET.SubElement(addData, 'Identifier')
            spuid = ET.SubElement(id, 'SPUID')
            spuid.set('spuid_namespace',"CDC-OAMD")
            spuid.text = xml['contractor_virus_name_ncbi']


        for index, xml in xml_test.iterrows():
            action3 = ET.SubElement(submission, 'Action')
            addfile = ET.SubElement(action3, 'AddFiles')
            addfile.set('target_db',"SRA")
            if self.ftype == 'fastq':
                file = ET.SubElement(addfile, 'File')
                file.set('cloud_url', xml['filename'])
                datatype=ET.SubElement(file, "DataType")
                datatype.text = 'generic-data'
                file = ET.SubElement(addfile, 'File')
                file.set('cloud_url', xml['filename2'])
                datatype=ET.SubElement(file, "DataType")
                datatype.text = self.ftype
            else:
                file = ET.SubElement(addfile, 'File')
                file.set('cloud_url', xml['filename'])
                datatype=ET.SubElement(file, "DataType")
                datatype.text = 'generic-data'
                Attr_assem = ET.SubElement(addfile, 'Attribute')
                Attr_assem.set('name',"assembly")
                Attr_assem.text = "NC_045512.2"
            datatype=ET.SubElement(file, "DataType")
            datatype.text = 'generic-data'
            Attr_model = ET.SubElement(addfile, 'Attribute')
            Attr_model.set('name',"instrument_model")
            Attr_model.text = xml['contractor_sequence_machine']
            Attr_name = ET.SubElement(addfile, 'Attribute')
            Attr_name.set('name',"library_name")
            Attr_name.text = xml['contractor_virus_name_ncbi']
            Attr_strat = ET.SubElement(addfile, 'Attribute')
            Attr_strat.set('name',"library_strategy")
            Attr_strat.text = xml['sampling_strategy']
            Attr_source = ET.SubElement(addfile, 'Attribute')
            Attr_source.set('name',"library_source")
            Attr_source.text = 'VIRAL RNA'
            Attr_select = ET.SubElement(addfile, 'Attribute')
            Attr_select.set('name',"library_selection")
            Attr_select.text = 'RT-PCR'
            Attr_lay = ET.SubElement(addfile, 'Attribute')
            Attr_lay.set('name',"library_layout")
            Attr_lay.text = 'PAIRED'
            Attr_prot = ET.SubElement(addfile, 'Attribute')
            Attr_prot.set('name',"library_construction_protocol")
            Attr_prot.text = xml['library_protocol']
            Attr_ref = ET.SubElement(addfile, 'AttributeRefId')
            Attr_ref.set("name","BioProject")
            refid = ET.SubElement(Attr_ref, "RefId")
            primid = ET.SubElement(refid, 'PrimaryId')
            primid.text = "PRJNA731149"
            Attr_ref = ET.SubElement(addfile, 'AttributeRefId')
            Attr_ref.set("name","BioSample")
            refid = ET.SubElement(Attr_ref, "RefId")
            spuid = ET.SubElement(refid, 'SPUID')
            spuid.set('spuid_namespace',"CDC-OAMD")
            spuid.text = xml['contractor_virus_name_ncbi']
            id = ET.SubElement(addfile, 'Identifier')
            spuid = ET.SubElement(id, 'SPUID')
            spuid.set('spuid_namespace',"CDC-OAMD")
            spuid.text = xml['contractor_vendor_id']

        return(ET.tostring(submission, pretty_print=True).decode())
